package com.company;

import java.util.Scanner;

public class hero {

    String name;
    int hp, mp, atk, def, agi, INT;
    int a;
    int skilldmg;
    int skillflag;
    int warriorskill = 0;
    int robberskill=0;
    boolean mps=false;
    boolean skilldh = false;
    boolean skilltf = false;
    boolean healflag=false;
    int healponit;
    String healname="";
    int maxhp,maxmp;
    warrior warrior;
    archer archer;
    mage mage;
    priest priest;
    robber robber;
    String[] skill = new String[2];
    int prsh = 200;

    public hero() {
        name = "";
    }

    public void heroch(int a, String name) {
        this.a = a;
        switch (a) {
            case 1:
                //warrior warrior1;
                warrior = new warrior(name);
                //warrior=warrior1;
                this.name = name;
                warrior.property(hp, mp, atk, def, agi, INT);
                maxhp=warrior.hp;
                hp = warrior.hp;
                maxmp=warrior.mp;
                mp = warrior.mp;
                atk = warrior.atk;
                def = warrior.def;
                agi = warrior.agi;
                INT = warrior.INT;
                skill[0] = "嘲諷";
                break;
            case 2:
                // archer archer;
                archer = new archer(name);
                this.name = name;
                archer.property(hp, mp, atk, def, agi, INT);
                maxhp=archer.hp;
                hp = archer.hp;
                maxmp=archer.mp;
                mp = archer.mp;
                atk = archer.atk;
                def = archer.def;
                agi = archer.agi;
                INT = archer.INT;
                skill[0] = "狙擊";
                skill[1] = "流星雨";
                // skilldmg=archer.skill1();
                break;
            case 3:
                //  mage mage;
                mage = new mage(name);
                this.name = name;
                mage.property(hp, mp, atk, def, agi, INT);
                maxhp=mage.hp;
                hp = mage.hp;
                maxmp=mage.mp;
                mp = mage.mp;
                atk = mage.atk;
                def = mage.def;
                agi = mage.agi;
                INT = mage.INT;
                skill[0] = "火球術";
                skill[1] = "火焰爆破";
                break;
            case 4:
                // robber robber;
                robber = new robber(name);
                this.name = name;
                robber.property(hp, mp, atk, def, agi, INT);
                maxhp=robber.hp;
                hp = robber.hp;
                maxmp=robber.mp;
                mp = robber.mp;
                atk = robber.atk;
                def = robber.def;
                agi = robber.agi;
                INT = robber.INT;
                skill[0] = "潛行";
                break;

            case 5:
                // priest priest;
                priest = new priest(name);
                this.name = name;
                priest.property(hp, mp, atk, def, agi, INT);
                maxhp=priest.hp;
                hp = priest.hp;
                maxmp=priest.mp;
                mp = priest.mp;
                atk = priest.atk;
                def = priest.def;
                agi = priest.agi;
                INT = priest.INT;
                break;
        }
    }
    public void skilluse() {
        switch (this.a) {
            case 1:
                skilldh = warrior.skillcall();
                skilldmg = warrior.skilllist();
                skillflag = warrior.skl;
                if (skillflag == 1)
                    warriorskill = 3;
                mp = warrior.mp;
                if(warrior.mps)
                {
                    warrior.mps=false;
                    mps=true;
                    System.out.println("魔力不足");
                }
                break;
            case 2:
                //  archer archer=new archer();
                skilldh = archer.skillcall();
                skilldmg = archer.skilllist();
                skillflag = archer.skl;
                mp = archer.mp;
                if(archer.mps)
                {
                    archer.mps=false;
                    mps=true;
                    System.out.println("魔力不足");
                }
                break;
            case 3:
                // mage mage=new mage();
                skilldh = mage.skillcall();
                skilldmg = mage.skilllist();
                skillflag = mage.skl;
                mp = mage.mp;
                if(mage.mps)
                {
                    mage.mps=false;
                    mps=true;
                    System.out.println("魔力不足");
                }
                break;
            case 4:
                // robber robber=new robber();
                skilldh = robber.skillcall();
                skilldmg = robber.skilllist();
                skillflag = robber.skl;
                mp = robber.mp;
                if(robber.mps)
                {
                    robber.mps=false;
                    mps=true;
                    System.out.println("魔力不足");
                }
                break;
            case 5:
                // priest priest=new priest();
                skilldh = priest.skillcall();
                skilldmg = priest.skilllist();
                skillflag = priest.skl;
              //  hp += skilldmg;

                mp = priest.mp;
                if(priest.mps)
                {
                    priest.mps=false;
                    mps=true;
                    System.out.println("魔力不足");
                }

                break;

        }
    }

    public boolean rooberskill() {
        //  robber robber=new robber();
        return robber.skill2();
    }
    public  void priestskill(int gg)
    {
        if(healflag && hp+gg<=maxhp)
            hp+=gg;
        else if (healflag)
            hp=maxhp;
        healflag=false;
    }
    public String skilltext() {
        switch (this.a) {
            case 1:
                if (skillflag == 1)
                    return name + "使用了嘲諷";
                else
                    return "\n";
            case 2:
                return "\n";
            case 3:
                return "\n";
            case 4:
                if (skillflag == 1) {
                    robberskill=3;
                    return name + "使用了潛行";
                }
                else
                    return "\n";
            case 5:
                if (skillflag == 1)
                    return  "因為"+ name+"回復了" + healponit + "點血量";
                else
                    return "\n";
            default:
                return "\n";
        }
    }
}








