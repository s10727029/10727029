package com.company;

import java.util.Scanner;

public class mage extends hero{
    private String name;
    public int hp,mp,atk,def,agi,INT;
    public int skl;
    boolean mps=false;
    public mage()
    {
        this("Mage");
    }
    public mage(String name)
    {
        this.name=name;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name=name;
    }
    public void property(int hp,int mp,int atk,int def,int agi,int INT)
    {
         hp=300;
        mp=300;
         atk=60;
         def=70;
         agi=50;
         INT=140;
        this.hp=hp;
        this.mp=mp;
        this.atk=atk;
        this.def=def;
        this.agi=agi;
        this.INT=INT;
        System.out.print(name+"\nHP:"+hp+"\nMP:"+mp+"\nATK:"+atk+"\nDEF:"+def+"\nAGI:"+agi+"\nINT:"+INT+"\n特殊技能:火球術,火焰爆破");
    }
    public boolean skillcall()
    {
        System.out.print("\n請選擇要使用的技能\n 1.火球術  40mp\n2.火焰爆破  70mp\n");
        Scanner B=new Scanner(System.in);
        skl=B.nextInt();
        switch (skl)
        {
            case 1:
                return true;
            case 2:
                return true;
            default:
                return true;
        }
    }
    public  int skilllist()
    {
        switch (skl)
        {
            case 1:
                return skill1();
            case 2:
                return skill2();
            default:
                return 0;
        }
    }
   public int skill1()
   {
       int atkdmg=0;
       if(mp>=40) {
           this.mp -= 40;
            atkdmg=INT*3;
       }
       else
       {
           mps=true;
       }
       return atkdmg;
   }
   public int skill2() {
       int skilldmg = 0;
       if (mp >= 70) {
           this.mp -= 70;
           skilldmg=2*INT;
       } else
       {
           mps=true;
       }
           return skilldmg;
   }
}
