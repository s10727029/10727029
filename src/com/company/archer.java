package com.company;

import java.util.Scanner;

public class archer extends hero{
    private String name;
    public int hp,mp,atk,def,agi,INT;
    public int skl;
    boolean mps=false;
    public archer()
    {
        this("Archer");
    }
    public archer(String name)
    {
        this.name=name;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name=name;
    }
    public void property(int hp,int mp,int atk,int def,int agi,int INT)
    {
         hp=350;
         mp=200;
         atk=140;
         def=80;
         agi=110;
         INT=40;
        this.hp=hp;
        this.mp=mp;
        this.atk=atk;
        this.def=def;
        this.agi=agi;
        this.INT=INT;//"\nAGI:"+agi+
        System.out.print(name+":\nHP:"+hp+"\nMP:"+mp+"\nATK:"+atk+"\nDEF:"+def+"\nAGI:"+agi+"\nINT:"+INT+"\n特殊技能:狙擊,流星雨");
    }
    public boolean skillcall()
    {
        System.out.print("\n請選擇要使用的技能\n 1.狙擊  30mp\n2.流星雨  40mp\n");
        Scanner b=new Scanner(System.in);
        skl=b.nextInt();
        switch (skl)
        {
            case 1:
                return true;
            case 2:
                return true;
            default:
                return false;
        }

    }
    public int skilllist()
    {


        switch (skl)
        {
            case 1:
                return skill1();

            case 2:
                return  skill2();
            default:
                return 0;

        }
    }
    public  int skill1()
    {

        int atkdmg=0;
        if(mp>=30) {
            this.mp -= 30;
            atkdmg = atk * 2;
        }
        else
            mps=true;

        return atkdmg;
    }
    public int skill2()
    {

        int atkdmg=0;
        if(mp>=40) {
            this.mp -= 40;
            atkdmg = (atk * 3) / 2;
        }
        else
            mps= true;
        return  atkdmg;
    }
}
